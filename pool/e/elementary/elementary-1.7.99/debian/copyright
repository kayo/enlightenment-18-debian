Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: EFL
Source: http://enlightenment.org/

Files: src/*/eina/*
Copyright: Jorge Luis "turran" Zapata <jorgeluis.zapata@gmail.com>
           Carsten Haitzler <raster@rasterman.com>
           Corey "atmos" Donohoe <atmos@atmos.org>
           Vincent "caro" Torri  <vtorri at univ-evry dot fr>
           Gustavo Sverzut Barbieri <barbieri@gmail.com>
           Tilman Sauerbeck <tilman@code-monkey.de>
           Cedric Bail <cedric.bail@free.fr>
           Peter "pfritz" Wehrfritz <peter.wehrfritz@web.de>
           Arnaud de Turckheim "quarium" <quarium@gmail.com>
           Alexandre "diaxen" Becoulet <diaxen@free.fr>
           Albin "Lutin" Tonnerre <albin.tonnerre@gmail.com>
           Andre Dieb <andre.dieb@gmail.com>
           Raphael Kubo da Costa <kubo@profusion.mobi>
           Gustavo Chaves <glima@profusion.mobi>
           Fabiano Fidêncio <fidencio@profusion.mobi>
           Brett Nash <nash@nash.id.au>
           Tom Hacohen <tom@stosb.com>
           Sebastian Dransfeld <sd@tango.flipp.net>
           Myungjae Lee <mjae.lee@samsung.com>
           Youness Alaoui <kakaroto@kakaroto.homelinux.net>
           billiob (Boris Faure) <billiob@gmail.com>
           Sung W. Park <sungwoo@gmail.com>
           Guillaume Friloux <guillaume.friloux@gmail.com>
           Jonas M. Gastal <jgastal@profusion.mobi>
           Raphael Kubo da Costa <rakuco@freebsd.org>
           Jérôme Pinot <ngc891@gmail.com>
           Mike Blumenkrantz <michael.blumenkrantz@gmail.com>
           Patryk Kaczmarek <patryk.k@samsung.com>
           Igor Murzov <e-mail@date.by>
           Vladislav Brovko <v.brovko@samsung.com>
License: LGPL-2.1

Files: src/*/eet/*
Copyright: The Rasterman (Carsten Haitzler) <raster@rasterman.com>
           David Goodlad <dgoodlad@gmail.com>
           Cedric Bail <cedric.bail@free.fr>
           Arnaud de Turckheim <quarium@gmail.com>
           Luis Felipe Strano Moraes <lfelipe@profusion.mobi>
           Chidambar Zinnoury <illogict@online.fr>
           Vincent Torri <vtorri@univ-evry.fr>
           Gustavo Sverzut Barbieri <barbieri@profusion.mobi>
           Raphael Kubo da Costa <kubo@profusion.mobi>
           Mathieu Taillefumier <mathieu.taillefumier@free.fr>
           Albin "Lutin" Tonnerre <albin.tonnerre@gmail.com>
           Adam Simpkins <adam@adamsimpkins.net>
           Mike Blumenkrantz <michael.blumenkrantz@gmail.com>
           Lionel Orry <lionel.orry@gmail.com>
           Jérôme Pinot <ngc891@gmail.com>
           Leandro Santiago <leandrosansilva@gmail.com>
License: BSD

Files: src/*/eo/*
Copyright: Tom Hacohen <tom@stosb.com>
License: BSD

Files: src/*/evas/*
Copyright: Carsten Haitzler <raster@rasterman.com>
           Till Adam <till@adam-lilienthal.de>
           Steve Ireland <sireland@pobox.com>
           Brett Nash <nash@fluffyspider.com.au>
           Tilman Sauerbeck <tilman@code-monkey.de>
           Corey Donohoe <atmos@atmos.org>
           Yuri Hudobin <glassy_ape@users.sourceforge.net>
           Nathan Ingersoll <ningerso@d.umn.edu>
           Willem Monsuwe <willem@stack.nl>
           Jose O Gonzalez <jose_ogp@juno.com>
           Bernhard Nemec <Bernhard.Nemec@viasyshc.com>
           Jorge Luis Zapata Muga <jorgeluis.zapata@gmail.com>
           Cedric Bail <cedric.bail@free.fr>
           Gustavo Sverzut Barbieri <barbieri@gmail.com>
           Vincent Torri <vtorri at univ-evry dot fr>
           Tim Horton <hortont424@gmail.com>
           Tom Hacohen <tom@stosb.com>
           Mathieu Taillefumier <mathieu.taillefumier@free.fr>
           Iván Briano <ivan@profusion.mobi>
           Gustavo Lima Chaves <glima@profusion.mobi>
           Samsung Electronics
           Samsung SAIT
           Sung W. Park <sungwoo@gmail.com>
           Jiyoun Park <jy0703.park@samsung.com>
           Myoungwoon Roy Kim(roy_kim) <myoungwoon.kim@samsung.com> <myoungwoon@gmail.com>
           Thierry el Borgi <thierry@substantiel.fr>
           Shilpa Singh <shilpa.singh@samsung.com> <shilpasingh.o@gmail.com>
           ChunEon Park <hermet@hermet.pe.kr>
           Christopher 'devilhorns' Michael <cpmichael1@comcast.net>
           Seungsoo Woo <om101.woo@samsung.com>
           Youness Alaoui <kakaroto@kakaroto.homelinux.net>
           Jim Kukunas <james.t.kukunas@linux.intel.com>
           Nicolas Aguirre <aguirre.nicolas@gmail.com>
           Rafal Krypa <r.krypa@samsung.com>
           Hyoyoung Chang <hyoyoung@gmail.com>
           Jérôme Pinot <ngc891@gmail.com>
           Rafael Antognolli <antognolli@profusion.mobi>
           Daniel Zaoui <daniel.zaoui@samsung.com>
           Christophe Sadoine <chris@indefini.org>
           Igor Murzov <e-mail@date.by>
           Sohyun Kim <anna1014.kim@samsung.com>
           Boris Faure <billiob@gmail.com>
           Eduardo Lima (Etrunko) <eblima@gmail.com>
           Leandro Dorileo (dorileo) <dorileo@profusion.mobi>
           Patryk Kaczmarek <patryk.k@samsung.com>
           Zbigniew Kosinski <z.kosinski@samsung.com>
           Paulo Cavalcanti <paulo.cavalcanti@linux.intel.com>
           Jean-Philippe Andre <jp.andre@samsung.com>
           Yury Usischev <y.usishchev@samsung.com>
License: BSD

Files: src/*/ecore/*
Copyright: The Rasterman <raster@rasterman.com>
           Tom Gilbert <tom@linuxbrit.co.uk>
           Burra <burra@colorado.edu>
           Chris Ross <chris@darkrock.co.uk>
           Term <term@twistedpath.org>
           Tilman Sauerbeck <tilman@code-monkey.de>
           Ibukun Olumuyiwa <ibukun@computer.org>
           Yuri <da2001@hotmail.ru>
           Nicholas Curran <quasar@bigblue.net.au>
           Howell Tam <pigeon@pigeond.net>
           Nathan Ingersoll <rbdpngn@users.sourceforge.net>
           Andrew Elcock <andy@elcock.org>
           Kim Woelders <kim@woelders.dk>
           Sebastian Dransfeld <sd@tango.flipp.net>
           Simon Poole <simon.armlinux@themalago.net>
           Jorge Luis Zapata Muga <jorgeluis.zapata@gmail.com>
           dan sinclair <zero@everburning.com>
           Michael 'Mickey' Lauer <mickey@tm.informatik.uni-frankfurt.de>
           David 'onefang' Seikel <onefang@gmail.com>
           Hisham 'CodeWarrior' Mardam Bey <hisham@hisham.cc>
           Brian 'rephorm' Mattern <rephorm@rephorm.com>
           Tim Horton <hortont424@gmail.com>
           Arnaud de Turckheim 'quarium' <quarium@gmail.com>
           Matt Barclay <mbarclay@gmail.com>
           Peter Wehrfritz <peter.wehrfritz@web.de>
           Albin "Lutin" Tonnerre <albin.tonnerre@gmail.com>
           Vincent Torri <vincent.torri@gmail.com>
           Lars Munch <lars@segv.dk>
           Andre Dieb <andre.dieb@gmail.com>
           Mathieu Taillefumier <mathieu.taillefumier@free.fr>
           Rui Miguel Silva Seabra <rms@1407.org>
           Samsung Electronics <tbd>
           Samsung SAIT <tbd>
           Nicolas Aguirre <aguirre.nicolas@gmail.com>
           Brett Nash <nash@nash.id.au>
           Mike Blumenkrantz <michael.blumenkrantz@gmail.com>
           Leif Middelschulte <leif.middelschulte@gmail.com>
           Mike McCormack <mikem@ring3k.org>
           Sangho Park <gouache95@gmail.com>
           Jihoon Kim <jihoon48.kim@samsung.com> <imfine98@gmail.com>
           PnB <Poor.NewBie@gmail.com>
           Daniel Juyung Seo <seojuyung2@gmail.com> <juyung.seo@samsung.com>
           Christopher 'devilhorns' Michael <cpmichael1@comcast.net>
           ChunEon Park (Hermet) <hermet@hermet.pe.kr>
           xlopez@igalia.com
           Rafael Antognolli <antognolli@profusion.mobi>
           Kim Yunhan <spbear@gmail.com>
           Youness Alaoui <kakaroto@kakaroto.homelinux.net>
           Bluezery <ohpowel@gmail.com>
           Doyoun Kang <wayofmine@gmail.com> <doyoun.kang@samsung.com>
           Haifeng Deng <haifeng.deng@samsung.com>
           Jérémy Zurcher <jeremy@asynk.ch>
           Seong-ho Cho (DarkCircle) <darkcircle.0426@gmail.com>
           Patryk Kaczmarek <patryk.k@samsung.com>
           Daniel Willmann <d.willmann@samsung.com>
           Michal Pakula vel Rutka <m.pakula@samsung.com>
           Vikram Narayanan <vikram186@gmail.com>
           Daniel Zaoui <daniel.zaoui@samsung.com>
           Yakov Goldberg <yakov.g@samsung.com>
           Eduardo Lima (Etrunko) <eblima@gmail.com>
           Alexey Yakovenko <wakeroid@gmail.com>
           Myoungwoon Roy Kim(roy_kim) <myoungwoon.kim@samsung.com> <myoungwoon@gmail.com>
           Robert David <robert.david.public@gmail.com>
           Gwanglim Lee <gl77.lee@samsung.com> <gwanglim@gmail.com>
           Sung-Jin Park <sj76.park@samsung.com>
           Guillaume Friloux <guillaume.friloux@gmail.com>
License: BSD

Files: src/*/embryo/*
Copyright: The Rasterman (Carsten Haitzler) <raster@rasterman.com>
           Jérôme Pinot <ngc891@gmail.com>
License: SMALL

Files: src/*/eio/*
Copyright: Cedric Bail <cedric.bail@free.fr>
           Stephen Houston <UnixTitan@gmail.com>
           Gustavo Sverzut Barbieri <barbieri@gmail.com>
           Vincent "caro" Torri <vtorri at univ-evry dot fr>
           Mikael SANS <sans.mikael@gmail.com>
           Mike Blumenkrantz (zmike/discomfitor) <michael.blumenkrantz@gmail.com>
           Jérôme Pinot <ngc891@gmail.com>
           Daniel Willmann <d.willmann@samsung.com>
           Jérémy Zurcher <jeremy@asynk.ch>
License: LGPL-2.1

Files: src/*/eldbus/*
Copyright: Gustavo Sverzut Barbieri <barbieri@profusion.mobi>
           José Roberto de Souza <zehortigoza@profusion.mobi>
           Leandro Pereira <leandro@profusion.mobi>
           Lucas De Marchi <lucas.demarchi@profusion.mobi>
License: LGPL-2.1

Files: src/*/efreet/*
Copyright: dan 'dj2' sinclair
           Brian 'rephorm' Mattern
           Sebastian Dransfeld <sd@tango.flipp.net>
           Nathan 'RbdPngn' Ingersoll
           Nicholas 'Mekius' Hughart
           Albin "Lutin" Tonnerre <albin.tonnerre@gmail.com>
           Mathieu Taillefumier <mathieu.taillefumier@free.fr>
License: BSD

Files: src/*/eeze/*
Copyright: Mike Blumenkrantz (zmike/discomfitor) <michael.blumenkrantz@gmail.com>
           Cedric Bail <cedric@efl.so>
           Mikael Sans <sans.mikael@gmail.com>
           Christophe Dumez <christophe.dumez@intel.com>
           Stefan Schmidt <stefan@datenfreihafen.org>
License: BSD

Files: src/*/ephysics/*
Copyright: Bruno Dilly <bdilly@profusion.mobi>
           Leandro Dorileo <dorileo@profusion.mobi>
           Ricardo de Almeida Gonzaga <ricardo@profusion.mobi>
License: BSD

Files: src/*/edje/*
Copyright: The Rasterman (Carsten Haitzler) <raster@rasterman.com>
           Tilman Sauerbeck (tilman at code-monkey de)
           ZigsMcKenzie <zigsmckenzie@gmail.com>
           Cedric BAIL <cedric.bail@free.fr>
           Brian Mattern <rephorm@rephorm.com>
           Mathieu Taillefumier <mathieu.taillefumier@free.fr>
           Tristan <blunderer@gmail.com>
           Gustavo Lima Chaves <glima@profusion.mobi>
           Bruno Dilly <bdilly@profusion.mobi>
           Fabiano Fidêncio <fidencio@profusion.mobi>
           Jihoon Kim <jihoon48.kim@samsung.com>
           Tiago Falcão <tiago@profusion.mobi>
           Davide Andreoli <dave@gurumeditation.it>
           Sebastian Dransfeld <sd@tango.flipp.net>
           Tom Hacohen <tom@stosb.com>
           Aharon Hillel <a.hillel@partner.samsung.com>
           Shilpa Singh <shilpa.singh@samsung.com> <shilpasingh.o@gmail.com>
           Mike Blumenkrantz <michael.blumenkrantz@gmail.com
           Jaehwan Kim <jae.hwan.kim@samsung.com>
           billiob (Boris Faure) <billiob@gmail.com>
           Govindaraju SM <govi.sm@samsung.com> <govism@gmail.com>
           Prince Kumar Dubey <prince.dubey@samsung.com> <prince.dubey@gmail.com>
           David Seikel <onefang at gmail.com>
           Mikael Sans <sans.mikael@gmail.com>
           Jérôme Pinot <ngc891@gmail.com>
           Rajeev Ranjan (Rajeev) <rajeev.r@samsung.com> <rajeev.jnnce@gmail.com>
           ChunEon Park (Hermet) <hermet@hermet.pe.kr>
           Kim Shinwoo <kimcinoo.efl@gmail.com>
           Michael Bouchaud (yoz) <michael.bouchaud@gmail.com>
           WooHyun Jung (woohyun) <woohyun0705@gmail.com>
           Guilherme Silveira <xguiga@gmail.com>
           Flavio Ceolin <flavio.ceolin@profusion.mobi>
           Daniel Zaoui <daniel.zaoui@yahoo.com>
           Daniel Willmann <d.willmann@samsung.com>
           Robert David <robert.david.public@gmail.com>
           Bluezery <ohpowel@gmail.com>
License: BSD

Files: src/bin/edje/epp/*
Copyright: 2003-2011 Kim Woelders
License: GPL-2

Files: src/*/emotion/*
Copyright: Carsten Haitzler <raster@rasterman.com>
           Vincent Torri <torri@maths.univ-evry.fr>
           Nicolas Aguirre <aguirre.nicolas@gmail.com>
           Sebastian Dransfeld <sd@tango.flipp.net>
           Cedric Bail <cedric.bail@free.fr>
           Rafael Antognolli <antognolli@profusion.mobi>
           Jérôme Pinot <ngc891@gmail.com>
           Pierre Le Magourou <pierre.lemagourou@openwide.fr>
           Hugo Camboulive <hugo.camboulive@zodiacaerospace.com>
           Sohyun Kim <anna1014.kim@samsung.com>
           Leandro Dorileo <dorileo@profusion.mobi>
License: BSD

Files: src/*/ethumb/*
Copyright: Rafael Antognolli <antognolli@profusion.mobi>
           Cedric Bail <cedric.bail@free.fr>
           Mikael SANS <sans.mikael@gmail.com>
           Jérôme Pinot <ngc891@gmail.com>
License: LGPL-2.1

Files: debian/*
Copyright: 2013 Kayo Phoenix <kayo@illumium.org>
License: GPL-3.0+

License: BSD
 Copyright (C) 2002-2012 Carsten Haitzler, Dan Sinclair, Mike Blumenkrantz,
 Samsung Electronics and various contributors (see AUTHORS)
 .
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 On Debian systems, the complete text of the BSD License can be found in
 "/usr/share/common-licenses/BSD".

License: GPL-2
 Copyright (C) 2008-2011 Jorge Luis Zapata, Cedric Bail, ProFUSION embedded
 systems, Samsung Electronics and various contributors (see AUTHORS)
 .
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1
 Copyright (C) 2008-2011 Jorge Luis Zapata, Cedric Bail, ProFUSION embedded
 systems, Samsung Electronics and various contributors (see AUTHORS)
 .
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: GPL-3.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in "/usr/share/common-licenses/GPL-3".
