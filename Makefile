arch:=amd64 armhf
dist:=experimental

dir:=dists/$(dist)/main
bpk=$(dir)/binary-$(1)/Packages
spk=$(dir)/source/Sources

gz=gzip -9c $(1) > $(1).gz
bz2=bzip2 -9c $(1) > $(1).bz2

root:=file://$(PWD)

help:
	@echo 'Available targets:'
	@echo 'rescan           - rescan source and all binary packages'
	@echo 'rescan-source    - rescan source packages'
	@echo 'rescan-binary    - rescan all binary packages'
	@echo 'rescan-ARCH      - rescan binary for specific ARCH, where ARCH is one of $(arch)'
	@echo 'all              - rescan and update'
	@echo 'clean            - cleans previously generated package indexes'
	@echo 'source-list      - prints sources.list content for this repo'
	@echo 'update           - runs apt-get update with sudo'

all: rescan update

rescan: $(addprefix rescan-,source $(arch))

rescan-source:
	@mkdir -p $(dir $(call spk,$*))
	@dpkg-scansources pool > $(call spk,$*)
	@$(call gz,$(call spk,$*))
	@$(call bz2,$(call spk,$*))

rescan-binary: $(addprefix rescan-,$(arch))

rescan-%:
	@mkdir -p $(dir $(call bpk,$*))
	@dpkg-scanpackages -a $* pool > $(call bpk,$*)
	@$(call gz,$(call bpk,$*))
	@$(call bz2,$(call bpk,$*))

clean:
	@rm -rf dists

source-list:
	@echo 'deb $(root) $(dist) main'
	@echo 'deb-src $(root) $(dist) main'

update:
	@sudo apt-get update
